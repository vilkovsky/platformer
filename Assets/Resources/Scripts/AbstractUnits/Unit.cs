﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : TypeObject
{
    public float scale = 1.0f;

 //   [SerializeField]
    protected GameObject parent;

    public LinksManager.Enemy enemy;

    public int lives = 1;

    // [HideInInspector]
    public int livesMax;

    public float speed = 1.0F;

    private GameObject heart;

    private MonsterLivesBar monsterLivesBar;

    protected virtual void Awake()
    {
        if (transform.tag != "Player")
//           Debug.Log(transform.name);
            parent = transform.parent.gameObject;

        livesMax = lives;
        transform.localScale = new Vector3(scale, scale);
    }

    //public enum Enemy
    //{
    //    your,
    //    enemy
    //}

    public virtual void ReceiveDamage()
    {
        monsterLivesBar = FindObjectOfType<MonsterLivesBar>();


        lives--;
        monsterLivesBar.Refresh(lives, livesMax);

        if (lives <= 0)
        {
            Die();
        }
    }

    protected virtual void Die()
    {


        // Тут надо все переделать
        float rand = Random.Range(0f, 5f);
        //    Debug.Log(rand);
        if (rand < 2)
        {
            heart = Resources.Load("Prefabs/Heart") as GameObject;
            GameObject newHeart = Instantiate(heart);
            newHeart.transform.position = gameObject.transform.position;
        }

        GameObject splash = Resources.Load("Prefabs/items/Splash") as GameObject;
        GameObject newSplash = Instantiate(splash);
        newSplash.transform.position = gameObject.transform.position;


        Destroy(parent);
    }

    public void StartSlowdown(float timeSlow, float speedSlow)
    {
        //  speed = speed - (speed / speedSlow);

        //     speed -= speedSlow;
        StartCoroutine(Slowdown(timeSlow, speed));
        speed = speed * (1 - speedSlow / 100);
    }

    IEnumerator Slowdown(float timeSlow, float speedSlow)
    {
        // Debug.Log("12342");
        // speed = 0;
        //   speed = speed - 0.5f;
        yield return new WaitForSeconds(timeSlow);
        speed = speedSlow;
        //    immortality = false;
        //  yield return null;
    }
}
