﻿using UnityEngine;

public class Shoot : MonoBehaviour
{
    public float blastRadius = 0.0f;

    public float gravityScale = 1.0f;
    // public float gravityScaleScatter = 0.0f;

    [SerializeField]
    protected Vector2 impulse;
    public Vector2 impulseScatter;

    protected Vector2 impulseHeight;


    public float interval = 1.0F;

    public float timeDestroy = 3.0f;

    [SerializeField]
    Vector2 translation;

    [SerializeField]
    private Color bulletColor = Color.white;

    [SerializeField]
    protected Bullet bullet;

    public Vector2 scale = new Vector2(1, 1);

    [SerializeField]
    protected float scaleAdd = 0;

    private Unit unit;

    //    private bool triggerShoot = true;

    protected Bullet newBullet;

    protected virtual void Start()
    {
        if (bullet == null)
        {
            bullet = LinksManager.bullet;
        }
        unit = GetComponent<Unit>();
    }

    //protected virtual void Start()
    //{
    //    triggerShoot = true;
    //}

    //protected virtual void Update()
    //{
    //    if (triggerShoot)
    //    {
    //        triggerShoot = false;
    //        StartCoroutine(AttackCoroutine());
    //    }
    //}

    //IEnumerator AttackCoroutine()
    //{
    //    yield return new WaitForSeconds(interval);
    //    Shooting();
    //    triggerShoot = true;
    //}

    //private void OnDisable()
    //{
    //    triggerShoot = true;
    //}

    public virtual void Shooting()
    {
        int direction = Lib.SetDirection(transform);
        Vector3 position = transform.position;
        position.x += translation.x * direction;
        position.y += translation.y;

        newBullet = Instantiate(bullet, position, transform.rotation);
        newBullet.color = bulletColor;
        newBullet.timeDestroy = timeDestroy;
        newBullet.enemy = unit.enemy;
        newBullet.scaleAdd = scaleAdd;
        newBullet.scale = scale * direction;

        newBullet.gravityScale = gravityScale;

        Rigidbody2D rb;
        rb = newBullet.GetComponent<Rigidbody2D>();

        float impulseX = Mathf.Abs(impulse.x+impulseHeight.x) * direction;

        Vector2 impulseRand;
        impulseRand.x = impulseX + Random.Range(-impulseScatter.x, impulseScatter.x);
        impulseRand.y = impulse.y+impulseHeight.y + Random.Range(-impulseScatter.y, impulseScatter.y);

        rb.AddForce(impulseRand, ForceMode2D.Impulse);
    }
}
