﻿using UnityEngine;

public class BulletMoveTowards : TypeObject
{
    public LinksManager.Enemy enemy;
    public float timeDestroy = 3.0F;
    public Vector2 scale = new Vector2(1, 1);
    public float scaleAdd;
 //   private float direction;
    public GameObject explosion;

    public float timeSlow = 0.0f;
    public float speedSlow = 0.0f;

    float offsetTargetY = 0.2f;
    float rand = 0.3f;

    [HideInInspector]
    public Vector3 target;
    public float speed = 2.5f;

    void Start()
    {
        transform.localScale = new Vector3(transform.localScale.x * scale.x, transform.localScale.y * scale.y, transform.localScale.z);
      //  direction = Lib.SetDirection(transform);
        Destroy(gameObject, timeDestroy);

        //       if (target == null)
        target = LinksManager.player.transform.position;
        target.x += Random.Range(-rand, rand);
        target.y += Random.Range(-rand, rand) + offsetTargetY;

        Vector3 vectorToTarget = target - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);


        if (explosion == null)
            explosion = LinksManager.explosion1;
    }

    void Update()
    {
    //    transform.localScale = new Vector3(transform.localScale.x + scaleAdd * direction, transform.localScale.y + scaleAdd * direction, 0);

        transform.Translate(Vector3.right * Time.deltaTime * speed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        TypeObject typeObject = collision.GetComponent<TypeObject>();

        if (typeObject && typeObject.obj == LinksManager.Obj.terrain)
        {
            Destroy(gameObject);
        }
        else
        {
            Unit unit = collision.GetComponent<Unit>();

            if (unit && unit.gameObject && unit.enemy != enemy)
            {
                if (speedSlow != 0)
                {
                    unit.StartSlowdown(timeSlow, speedSlow);
                }
                unit.ReceiveDamage();
                Destroy(gameObject);
            }
        }
    }

    private void OnDestroy()
    {
        Instantiate(explosion, transform.position, transform.rotation);
    }
}
