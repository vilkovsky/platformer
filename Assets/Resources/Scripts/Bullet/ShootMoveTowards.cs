﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootMoveTowards : MonoBehaviour
{

    public float timeDestroy = 3.0f;

    [SerializeField]
    Vector2 translation = new Vector2(0.2f, 0.3f);

    [SerializeField]
    protected BulletMoveTowards bulletMoveTowards;

    public Vector2 scale = new Vector2(1, 1);

    [SerializeField]
    protected float scaleAdd = 0;

    private Unit unit;

    protected virtual void Start()
    {
        if (bulletMoveTowards == null)
        {
            // bulletMoveTowards = LinksManager.bulletMoveTowards1;
             bulletMoveTowards = LinksManager.bulletMoveTowardsEmpty;
        }
        unit = GetComponent<Unit>();
    }

    public virtual void Shooting()
    {
        int direction = Lib.SetDirection(transform);
        Vector3 position = transform.position;
        position.x += translation.x * direction;
        position.y += translation.y;

        BulletMoveTowards newBullet = Instantiate(bulletMoveTowards, position, transform.rotation);
        newBullet.timeDestroy = timeDestroy;

        newBullet.enemy = unit.enemy;
        newBullet.scaleAdd = scaleAdd;
   //     newBullet.scale = scale * direction;
    }

    //   // Use this for initialization
    //   void Start () {

    //}

    //// Update is called once per frame
    //void Update () {

    //}
}
