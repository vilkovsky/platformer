﻿using UnityEngine;

public class Bullet : TypeObject

{
    public LinksManager.Enemy enemy;

    public float gravityScale = 1.0f;

    public float blastRadius = 0.0f;

    public float timeDestroy = 0.1F;

    public float timeSlow = 0.0f;
    public float speedSlow = 0.0f;

    public Vector2 scale;

    public float scaleAdd;

    private float direction;

    public Color color
    {
        set { sprite.color = value; }
    }

    private SpriteRenderer sprite;

    public GameObject explosion;

    void Awake()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();

        //if (explosion == null)
        //    explosion = LinksManager.explosion1;

    }

    private void Start()
    {
        if (explosion == null)
            explosion = LinksManager.explosion1;

        Rigidbody2D rigidbody2D = GetComponent<Rigidbody2D>();
        rigidbody2D.gravityScale = gravityScale;

        transform.localScale = new Vector3(transform.localScale.x * scale.x, transform.localScale.y * scale.y, transform.localScale.z);

        direction =Lib.SetDirection(transform);
        Destroy(gameObject, timeDestroy);
    }

    private void Update()
    {
        transform.localScale = new Vector3(transform.localScale.x + scaleAdd * direction, transform.localScale.y + scaleAdd * direction, 0);
    }

    //private int SetDirection()
    //{
    //    if (transform.localScale.x < 0)
    //    {
    //        return -1;
    //    }
    //    else
    //    {
    //        return 1;
    //    }
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        TypeObject typeObject = collision.GetComponent<TypeObject>();

        if (typeObject && typeObject.obj ==LinksManager.Obj.terrain)
        {
            Destroy(gameObject);
        }
        else
        {
            Unit unit = collision.GetComponent<Unit>();

            if (unit && unit.gameObject && unit.enemy != enemy)
            {
                if (speedSlow != 0)
                {
                    unit.StartSlowdown(timeSlow, speedSlow);
                }
                unit.ReceiveDamage();
                Destroy(gameObject);
            }
        }
    }

    private void OnDestroy()
    {
        if (blastRadius != 0)
        {
            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, blastRadius);
            for(var i = 0; i < colliders.Length; i++)
            {
                Unit unit = colliders[i].GetComponent<Unit>();
                if (unit && unit.gameObject && unit.enemy != enemy)
                {
                    unit.ReceiveDamage();
                }
            }

        }
   //     Debug.Log("123");
        Instantiate(explosion, transform.position, transform.rotation);
    }
}
