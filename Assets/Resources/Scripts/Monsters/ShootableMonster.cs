﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootableMonster : Monster {

    public int rotationX;

    protected override void Start()
    {

    }

    protected override void Update()
    {  
        Lib.RotationToPlayer(transform);
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        Unit unit = collision.GetComponent<Unit>();

        if (unit && unit is PlayerController)
        {
            if (Mathf.Abs(unit.transform.position.x - transform.position.x) < 0.1F)
            {
                ReceiveDamage();
            }
            else
            {
                unit.ReceiveDamage();
            }
        }
    }
}
