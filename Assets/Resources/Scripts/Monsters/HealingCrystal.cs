﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingCrystal : Unit
{

    public float healingRadius = 3.0f;
    public float healingInterval = 5.0f;

    [SerializeField]
    private float radiusScale = 0.1f;
    private float scaleStep = 0.1f;

    private float radMax;
    private float radMin;

    //// Use this for initialization
    void Start()
    {
        radMax = 1 + radiusScale;
        radMin = 1 - radiusScale;

        StartCoroutine(Healing());
    }

    //// Update is called once per frame
    void Update()
    {
        float xy = transform.localScale.x + scaleStep * Time.deltaTime;
        transform.localScale = new Vector3(xy, xy, 0);
        if (xy > radMax)
        {
            scaleStep = -scaleStep;
        }
        else if (xy < radMin)
        {
            scaleStep = -scaleStep;
        }

    }

    IEnumerator Healing()
    {
        while (true)
        {
            yield return new WaitForSeconds(healingInterval);

            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, healingRadius);
            for (var i = 0; i < colliders.Length; i++)
            {
                Unit unit = colliders[i].GetComponent<Unit>();
                if (unit && unit.enemy == enemy)
                {
                    if (unit.lives < unit.livesMax)
                    {
                        unit.lives += 1;
                        //     float height = unit.GetComponentInChildren<SpriteRenderer>().sprite.rect.height;
                        //  Instantiate(LinksManager.healingParticle, new Vector3(unit.transform.position.x,unit.transform.position.y+height,0),transform.rotation,unit.transform);

                       int layer = unit.GetComponentInChildren<SpriteRenderer>().sortingOrder;

                        GameObject particleHealing = Instantiate(LinksManager.healingParticle, unit.transform.position, transform.rotation, unit.transform);
                        particleHealing.GetComponentInChildren<ParticleSystemRenderer>().sortingOrder = layer + 1;
                    }
                }
            }
        }

    }
}
