﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.Collections;
using UnityEngine.UI;

public class Monster : Unit
{
    public bool touchDamage;

    private Animator animator;
    protected LinksManager.CharState charState
    {
        get { return (LinksManager.CharState)animator.GetInteger("State"); }
        set { animator.SetInteger("State", (int)value); }
    }

    [HideInInspector]
 //   public Vector3 direction;

 //   int way = 1;
    public NextLevel nextLevel;

    protected override void Die()
    {
        base.Die();
        if (nextLevel)
        {
            nextLevel.openDoor = true;
        }
    }

    protected virtual void Start()
    {
        animator = GetComponent<Animator>();

        Lib.MonsterSortLayer(GetComponentInChildren<SpriteRenderer>());
    }

    protected virtual void Update()
    {

    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        Unit unit = collision.GetComponent<Unit>();

        if (touchDamage && unit && unit is PlayerController)
        {
            //if (Mathf.Abs(unit.transform.position.x - transform.position.x) < 0.1F)
            //{
            //    ReceiveDamage();
            //}
            //else
            {
                unit.ReceiveDamage();
            }
        }
    }

    //public bool RotationToPlayer()
    //{
    //    if (transform.position.x - LinksManager.player.transform.position.x < 0)
    //    {
    //        if (way == -1)
    //        {
    //            way = 1;
    //     //       direction = -direction;
    //            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y);
    //            return true;
    //        }
    //        return false;
    //    }
    //    else
    //    {
    //        if (way == 1)
    //        {
    //            way = -1;
    //      //      direction = -direction;
    //            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y);
    //            return true;
    //        }
    //        return false;
    //    }
    //}

    //public void Move(float speed)
    //{
    //    transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.right * way, speed * Time.deltaTime);
    //}
}
