﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : Troll
{
    bool receiveDamage;

    protected override void Update()
    {
        base.Update();
        if (charState == LinksManager.CharState.Attack)
        {
            receiveDamage = true;
        }
        else
        {
            receiveDamage = false;
        }
    }

    public override void ReceiveDamage()
    {
        if (receiveDamage)
            base.ReceiveDamage();
    }


}
