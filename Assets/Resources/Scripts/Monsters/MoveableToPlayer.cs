﻿using UnityEngine;

public class MoveableToPlayer : MoveableMonster
{
    public float distanceWalkToPlayer = 0.1f;

    protected bool allowedToMove = true;

    protected override void Update()
    {
        Rotate();
        MoveCollision();
    }

    protected void MoveCollision()
    {
        Collider2D []
        colliders = CheckBarrier();
        if (colliders.Length != 0 && allowedToMove)
        {
            float distanceX = Mathf.Abs(transform.position.x - LinksManager.player.transform.position.x);
            if (distanceX > distanceWalkToPlayer)
            {
                Move();
}
            else
            {
                charState = LinksManager.CharState.Idle;
            }
        }
        else
        {
            charState = LinksManager.CharState.Idle;
        }
    }

    protected virtual void Rotate()
    {
        if (Lib.RotationToPlayer(transform))
        {
            allowedToMove = true;
        }
    }

    protected override void Turn()
    {
        allowedToMove = false;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);

        TypeObject typeObject = collision.GetComponent<TypeObject>();

        if (typeObject)
        {
            if (typeObject.obj == LinksManager.Obj.terrain || typeObject.obj == LinksManager.Obj.openBullet)
            {
                allowedToMove = true;
            }
        }
    }
}
