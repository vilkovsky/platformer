﻿using UnityEngine;

public class MoveableMonster : Monster
{
    protected override void Start()
    {
        base.Start();
   //     direction = transform.right;
    }

    protected override void Update()
    {
        CheckTurn(CheckBarrier());
        Move();
    }

    protected virtual void Move()
    {
      //  float dir=transform.localScale.x
        //parent.transform.position = Vector3.MoveTowards(parent.transform.position, parent.transform.position + direction, speed * Time.deltaTime);
        parent.transform.position = Vector3.MoveTowards(parent.transform.position, parent.transform.position +new Vector3(transform.localScale.x,0,0), speed * Time.deltaTime);
    }

    protected Collider2D[] CheckBarrier()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(parent.transform.position + parent.transform.up * -0.2F + parent.transform.right * transform.localScale.x * 0.2F, 0.01F);
        return colliders;
    }

    protected void CheckTurn(Collider2D[] colliders)
    {
        if (colliders.Length == 0)
        {
            Turn();
        }
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);

        TypeObject typeObject = collision.GetComponent<TypeObject>();

        if (typeObject)
        {
            if (typeObject.obj ==LinksManager.Obj.terrain || typeObject.obj ==LinksManager.Obj.openBullet)
            {
                Turn();
            }
        }

    }

    protected virtual void Turn()
    {
      //  direction = -direction;
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
}
