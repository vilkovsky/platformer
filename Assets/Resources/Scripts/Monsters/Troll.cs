﻿using System.Collections;
using UnityEngine;

public class Troll : MoveableToPlayer
{
    public float distanceAttack = 2.3f;

    public float rollbackTime = 3.0f;
    protected bool rollbackActivate;

    protected override void Update()
    {
        if (CheckDistanceToPlayer())
        {
                charState = LinksManager.CharState.Walk;
                base.Update();
        }
        else
        {
            if (rollbackActivate==false)
            {
                Attack();
            }
            else
            {
                //RollBackTime();
                charState = LinksManager.CharState.Idle;
            }
        }
    }

    //protected virtual void RollBackTime()
    //{
    //    charState = LinksManager.CharState.Idle;
    //}


    protected bool CheckDistanceToPlayer()
    {
        if (Lib.DistanceToPlayer(transform.position) < distanceAttack)
        {
            return false;
        }
        return true;
    }

    protected void Attack()
    {
        charState = LinksManager.CharState.Attack;
    }

    void Damage()
    {
        LinksManager.playerController.ReceiveDamage();
    }

    IEnumerator Rollback()
    {
        yield return new WaitForSeconds(rollbackTime);
        rollbackActivate = false;
    }

    void RollbackStart()
    {
        rollbackActivate = true;
        StartCoroutine(Rollback());
    }
}
