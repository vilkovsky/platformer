﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceWizard : Troll
{
    protected override void Update()
    {
        if (rollbackActivate)
        {
            charState = LinksManager.CharState.Walk;
            Lib.RotationFromPlayer(transform);
            MoveCollision();
        }
        else
        {
            base.Update();
            Lib.RotationToPlayer(transform);
        }
    }
}