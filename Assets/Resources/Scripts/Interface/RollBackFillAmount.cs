﻿using UnityEngine.UI;
using UnityEngine;

public class RollBackFillAmount : MonoBehaviour
{
    public bool timeout = true;

    private Image image;
    private float step = 0.03f;

    private void Start()
    {
        image = GetComponent<Image>();
    }

    public void RollbackStart(float time)
    {
        timeout = false;
        image.fillAmount = 0;
        InvokeRepeating("NewRollback", time, time);
    }

    private void NewRollback()
    {
        image.fillAmount += step;
        if (image.fillAmount >= 1)
        {
            timeout = true;
            CancelInvoke();
        }
    }
}
