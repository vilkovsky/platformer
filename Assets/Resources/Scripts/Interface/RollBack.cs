﻿using UnityEngine;
using UnityEngine.UI;

public class RollBack : MonoBehaviour
{
//    private Image image;

    public bool timeout = true;

    public float timeCounter = 10f;

    private float scale;

    private float step;

    //private void Start()
    //{
    //    image = GetComponent<Image>();
    //}

    public void RollbackStart(float time)
    {
        timeout = false;
        scale = 1;
        InvokeRepeating("NewScale", time, time);
    }

    void NewScale()
    {
        scale = scale - timeCounter;
        if (scale <= 0)
        {
            scale = 0;
            timeout = true;
            CancelInvoke();
        }
        transform.localScale = new Vector3(1, scale, 1);
    }

}
