﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class levelText : MonoBehaviour {

    [SerializeField]
    private float timeDestroy = 5.0f;

	void Start () {
        Text text = GetComponent<Text>();
        text.text = LinksManager.level;
        Destroy(gameObject, timeDestroy);
    }
}
