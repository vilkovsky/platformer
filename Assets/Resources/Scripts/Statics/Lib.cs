﻿using UnityEngine;

public class Lib : MonoBehaviour
{
    public static Lib instance = null;

    private const int monsterLayerMin=1;
    private const int monsterLayerMax=1000;
    private static int monsterLayer;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }

 //       DontDestroyOnLoad(gameObject);

        InitializeManager();
    }

    private void InitializeManager()
    {
        monsterLayer = monsterLayerMin;
    }

    public static int SetDirection(Transform transform)
    {
        if (transform.localScale.x < 0)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }

    public static bool RotationToPlayer(Transform transform)
    {
        if (transform.position.x - LinksManager.player.transform.position.x > 0)
        {
            if (transform.localScale.x > 0)
            { 
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y);
                return true;
            }
            return false;
        }
        else
        {
            if (transform.localScale.x < 0)
            {
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y);
                return true;
            }
            return false;
        }
    }

    public static bool RotationFromPlayer(Transform transform)
    {
        if (transform.position.x - LinksManager.player.transform.position.x < 0)
        {
            if (transform.localScale.x > 0)
            {
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y);
                return true;
            }
            return false;
        }
        else
        {
            if (transform.localScale.x < 0)
            {
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y);
                return true;
            }
            return false;
        }
    }

    public static float DistanceToPlayer(Vector3 pos)
    {
        return Vector3.Distance(pos, LinksManager.player.transform.position);
    }

    public static void MonsterSortLayer(SpriteRenderer spriteRenderer)
    {      
        spriteRenderer.sortingLayerName = "Monster";
        spriteRenderer.sortingOrder = monsterLayer;
          monsterLayer+=2;
        if (monsterLayer > monsterLayerMax) monsterLayer = monsterLayerMin;
    }

}
