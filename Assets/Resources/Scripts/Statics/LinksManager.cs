﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LinksManager : MonoBehaviour
{
    public static LinksManager instance = null;

    #region Global
    public static string level;

    public static Texture2D map1;

    public static GameObject platform1;

    public static float hideDistance = 8;
    GameObject poolDistancePref;
    public static int poolStep = 3;
    public static float poolTime = 0.5f;
    public static GameObject[] poolDistance = new GameObject[700];


    public static GameObject dialogPanel;
    public static Text dialogText;

    public static string fileNameIni = "game.ini";

    public static string filenameSaveLoad = "stream";

    public static GameObject player;
    public static PlayerController playerController;

    public static Bullet bullet;
    public static Bullet bullet1;
    public static Bullet bullet4;

    public static BulletMoveTowards bulletMoveTowards1;
    public static BulletMoveTowards bulletMoveTowardsEmpty;


    public static GameObject explosion1;
    public static GameObject explosion2;
    public static GameObject healingParticle;

 //   public static RawImage parallaxImage1;
    #endregion

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }

   //     DontDestroyOnLoad(gameObject);

        InitializeManager();
    }

    private void InitializeManager()
    {
        level = SceneManager.GetActiveScene().name;

        map1 = Resources.Load("Sprites/Maps/map") as Texture2D;
        platform1 = Resources.Load("Prefabs/Platform") as GameObject;

        AddPoolsDistance();

        dialogPanel = GameObject.Find("UpPanel/Dialog");
        dialogText = dialogPanel.GetComponentInChildren<Text>();
        dialogPanel.SetActive(false);

        player = GameObject.FindGameObjectWithTag("Player");
        playerController = player.GetComponent<PlayerController>();

        bullet = Resources.Load<Bullet>("Prefabs/items/Bullet");
        bullet1 = Resources.Load<Bullet>("Prefabs/items/BulletPlayer2");
        bullet4 = Resources.Load<Bullet>("Prefabs/items/iceSpear");

        bulletMoveTowards1= Resources.Load<BulletMoveTowards>("Prefabs/items/BulletMoveTowards");
        bulletMoveTowardsEmpty= Resources.Load<BulletMoveTowards>("Prefabs/items/EmptyMoveTowards");

        explosion1 = Resources.Load("Prefabs/items/Particles/particle1") as GameObject;
        explosion2 = Resources.Load("Prefabs/items/Particles/particle2") as GameObject;
        healingParticle= Resources.Load("Prefabs/items/Particles/HealingParticle") as GameObject;
    }

    private void AddPoolsDistance()
    {
        GameObject poolsDistance = new GameObject();
        poolsDistance.name = "poolsDistance";

        poolDistancePref = Resources.Load<GameObject>("Prefabs/items/poolDistance");
        for (var i = 0; i < poolDistance.Length; i++)
        {
            poolDistance[i] = Instantiate(poolDistancePref);
            poolDistance[i].transform.parent = poolsDistance.transform;
            Distance distance = poolDistance[i].GetComponent<Distance>();
            distance.reloadTime = i * poolTime + 1;
        }
    }

    public enum Enemy
    {
        your,
        enemy
    }

    public enum CharState
    {
        Idle,
        Walk,
        Jump,
        Attack,
        Shield
    }

    public enum Obj
    {
        monster,
        terrain,
        building,
        openBullet
    }
}