﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour
{
    public bool openDoor = true;

    public string nextLevel;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (openDoor == true && collision.gameObject.tag == "Player")
        {
            SceneManager.LoadScene(nextLevel);
            SaveLoad.Save(nextLevel);
        }
    }
}
