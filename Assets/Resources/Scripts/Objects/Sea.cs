﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sea : MonoBehaviour {

    float speed = 0.0f;
    float radius = 0.03f;
    Vector3 centr;

	void Start () {
        centr = transform.position;
	}

	void Update () {
        speed += Time.deltaTime;
        float x = centr.x + radius * Mathf.Cos(speed);
        float y = centr.y + radius * Mathf.Sin(speed);
        transform.position = new Vector3(x,y,transform.position.z);
	}
}
