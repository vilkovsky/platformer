﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : Unit
{
    [SerializeField]
    private float shieldRollBack = 0.1f;

    //public RollBack rollBack1;
    //public RollBack rollBack2;
    //public RollBack rollBack3;
    //public RollBack rollBack4;

    public RollBackFillAmount rollBack1;
    public RollBackFillAmount rollBack2;
    public RollBackFillAmount rollBack3;
    public RollBackFillAmount rollBack4;

    // [SerializeField]
    private float forceReceiveDamage = 2.0f;

    private ShootPlayer1 shootPlayer1;
    private ShootPlayer2 shootPlayer2;
    private ShootPlayer4 shootPlayer4;

    public float jump = 4f;

 //   Vector3 respawnPos;

    float veryBottom = -3.0F;

    public RawImage mountRawImage;
    //public RawImage seaRawImage;
    public float speedMount = 0.001F;
    //public float speedSea = 0.1F;
    // private float seaPosY;

    public GameObject shield;

    float impulseForward = 6.0f;
    float impulseDown = -4.0f;

    int scaleAttack;

    public int Lives
    {
        get { return lives; }
        set
        {
            if (value < 8) lives = value;
            livesBar.Refresh();
        }
    }
    private LivesBar livesBar;
//   private Rigidbody2D rb;
    private Animator animator;
    private bool isGrounded;
    //  private SpriteRenderer sprite;
    private Rigidbody2D rb;
    private Vector3 direction;
    private bool immortality;

    // private Bullet bullet;

    float posMount = 0;
 //   float posSea = 0;

    private CharState State
    {
        get { return (CharState)animator.GetInteger("State"); }
        set { animator.SetInteger("State", (int)value); }
    }

    void Start()
    {
        //     seaPosY = seaRawImage.transform.position.y;

        shootPlayer1 = GetComponent<ShootPlayer1>();
        shootPlayer2 = GetComponent<ShootPlayer2>();
        shootPlayer4 = GetComponent<ShootPlayer4>();
        

 //       respawnPos = transform.position;

        livesBar = FindObjectOfType<LivesBar>();
        //      sprite = GetComponentInChildren<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        //   bullet = Resources.Load<Bullet>("Prefabs/items/BulletPlayer2");

    //    rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
      //  Debug.Log(AudioListener.volume.ToString());

        if (Time.timeScale == 1 && !immortality)
        {
            if (Input.GetButton("Fire2"))
            {

                State = CharState.Shield;

                shield.SetActive(true);

                if (Input.GetButton("Fire1"))
                {
                    if (rollBack3.timeout)
                    {
                        rollBack3.RollbackStart(shieldRollBack);
                        rb.AddForce(transform.right * transform.localScale.x * impulseForward, ForceMode2D.Impulse);
                    }
                }

                if (Input.GetButton("Fire3"))
                {
                    if (rollBack3.timeout)
                    {
                        rollBack3.RollbackStart(shieldRollBack);
                        rb.AddForce(transform.right * transform.localScale.x * impulseDown, ForceMode2D.Impulse);
                    }
                }

            }
            else
            {
                shield.SetActive(false);


                if (isGrounded && !(Input.GetButton("Fire1")))
                {
                    State = CharState.Idle;
                }


                if (Input.GetButton("Horizontal"))
                    Run();

                if (Input.GetButtonDown("Jump") && isGrounded)
                {
                    Jump();
                }

                if (Input.GetButton("Fire1"))
                {
                    Shoot1();
                }
                else if (Input.GetButton("Fire3"))
                {
                    Shoot2();
                }
                else if (Input.GetKeyDown(KeyCode.Alpha1))
                {
                    Shoot4();
                }
            }

            CheckVeryBottom();
        }
    }

    private void CheckVeryBottom()
    {
        if (transform.position.y < veryBottom)
            ReceiveDamage();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "sea")
        {
            Die();
        }
    }

    void Shield()
    {
        shield.SetActive(true);
    }

    private void Shoot1()
    {
        if (rollBack1.timeout)
        {
            rollBack1.RollbackStart(shootPlayer1.interval);
            State = CharState.Attack;
            shootPlayer1.Shooting();
        }
        //if (rollBack1.timeout)
        //{
        //    rollBack1.RollbackStart(shootPlayer1.interval);
        //    State = CharState.Attack;
        //    shootPlayer1.Shooting();
        //}
    }

    private void Shoot2()
    {
        if (rollBack2.timeout)
        {
            rollBack2.RollbackStart(shootPlayer2.interval);
            State = CharState.Attack;
            shootPlayer2.Shooting();
        }
    }

    private void Shoot4()
    {
        if (rollBack4.timeout)
        {
            rollBack4.RollbackStart(shootPlayer4.interval);
            State = CharState.Attack;
            shootPlayer4.Shooting();
        }
    }

    private void FixedUpdate()
    {
        CheckGround();
    }

    void Run()
    {
        float right = Input.GetAxis("Horizontal");
        direction = transform.right * right;
        float run = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, run);

        if (right > 0) transform.localScale = new Vector3(1, 1, 1);
        else transform.localScale = new Vector3(-1, 1, 1); ;

        if (isGrounded)
            State = CharState.Run;

        Parallax(right);
    }

    void Parallax(float right)
    {
        posMount += speedMount * right * Time.deltaTime;
        if (posMount > 1.0F)
            posMount -= 1.0F;
        mountRawImage.uvRect = new Rect(posMount, 0, 1, 1);

        //  posSea += speedSea * right * Time.deltaTime;

        //float posSeaY= transform.position.y-seaPosY;

        // seaRawImage.transform.position=new Vector3(seaRawImage.transform.position.x,posSeaY);
        //seaRawImage.uvRect = new Rect(posSea, 0,1,1);
    }

    void Jump()
    {
        rb.AddForce(Vector2.up * jump, ForceMode2D.Impulse);
    }

    void CheckGround()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(new Vector3(transform.position.x - 0.02F, transform.position.y - 0.02F, transform.position.z), 0.02f);

        isGrounded = colliders.Length > 0;

        if (!isGrounded)
        {
            State = CharState.Jump;
        }
    }

    public enum CharState
    {
        Idle,
        Run,
        Jump,
        Attack,
        Shield
    }

    public override void ReceiveDamage()
    {
        if (!immortality)
        {
            Lives--;
            Instantiate(LinksManager.explosion1, transform.position, transform.rotation);
        }

        if (Lives <= 0)
        {
            Die();
        }
        else
        {
            StartCoroutine(RollbackReceiveDamage());
            immortality = true;

            rb.velocity = Vector3.zero;
            rb.AddForce(transform.up * forceReceiveDamage * 2, ForceMode2D.Impulse);
            rb.AddForce(transform.right * forceReceiveDamage * -direction.x, ForceMode2D.Impulse);
        }

    }

    protected override void Die()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //Lives = 7;
        //transform.position = respawnPos;
    }

    IEnumerator RollbackReceiveDamage()
    {
        yield return new WaitForSeconds(0.5f);
        immortality = false;
        //  yield return null;
    }
}
