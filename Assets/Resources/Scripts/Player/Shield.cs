﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    //private void Start()
    //{
    //    LinksManager.playerController.rollBack3.timeout;
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!LinksManager.playerController.rollBack3.timeout)
        {
            Unit unit = collision.GetComponent<Unit>();
            if (unit && unit.gameObject && unit.enemy != LinksManager.Enemy.your)
            {
                unit.ReceiveDamage();
            }
        }
    }
}
