﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
//[InitializeOnLoad]
[ExecuteInEditMode]

public class Map : MonoBehaviour
{
    public bool Do = false;

    GameObject platform;
    GameObject platformLeft;
    GameObject platformRight;


    Texture2D map;
    Color platformColor;


    private void Update()
    {
        if (Do)
        {
            DoMyWork();
            Do = false;
        }
    }

    private void DoMyWork()
    {
        for (var x = 0; x < map.width; x++)
        {
            for (var y = 0; y < map.height; y++)
            {
                if (map.GetPixel(x, y) == platformColor)
                {
                    AddNewPlatform(x, y);
                }
            }
        }
    }

    void AddNewPlatform(int x, int y)
    {
        // GameObject platform = LinksManager.platform1;




       
        Vector3 position = new Vector3(x, y, 0);
        //position.x = x;
        //position.y = y;
 //       GameObject newPlatform;
        switch (CheckDirectionPlatform(x, y))
        {
            case -1:
                Instantiate(platformLeft, position, transform.rotation);
                break;
            case 1:
                Instantiate(platformRight, position, transform.rotation);
                break;
            default:
                Instantiate(platform, position, transform.rotation);
                break;
        }

        //    newPlatform = Instantiate(platform, position, transform.rotation);
        //  newPlatform.transform.parent=
        //  Debug.Log(position);
    }

    int CheckDirectionPlatform(int x, int y)
    {
        // Left Up
        if (CheckPixel(x, y + 1)) return 0;

        if (!CheckPixel(x - 1, y + 1) && !CheckPixel(x - 1, y) && CheckPixel(x - 1, y - 1))
        {
            Debug.Log("Left");
            return -1;
        }
        else if (!CheckPixel(x + 1, y + 1) && !CheckPixel(x + 1, y) && CheckPixel(x + 1, y - 1))
        {
            Debug.Log("Right");
            return 1;
        }


        //if (CheckPixel(x - 1, y+1))
        //{
        //    Debug.Log("Left");
        //    return -1;
        //}
        //else if (CheckPixel(x + 1, y+1))
        //{
        //    Debug.Log("Right");
        //    return 1;
        //}


        Debug.Log("Centr");
        return 0;
    }

    bool CheckPixel(int x, int y)
    {
        //if (x < 0 || y < 0 || x >= map.width || y <= map.height)
        //{
        //    return false;
        //}
        return map.GetPixel(x, y) == platformColor;
    }

    void Start()
    {
        map = LinksManager.map1;
        map = Resources.Load("Sprites/Maps/map5") as Texture2D;

        platform = Resources.Load("Prefabs/Platform") as GameObject;
        platformLeft = Resources.Load("Prefabs/PlatformLeft") as GameObject;
        platformRight = Resources.Load("Prefabs/PlatformRight") as GameObject;

        platformColor = Color.black;


    }


}
